package vignesh.testcases;

import java.io.IOException;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import vignesh.excel.Exceldata;
import vignesh.pageobjects.Admin_Console;
import vignesh.pageobjects.LoginPage;
import vignesh.utils.Utilities;

public class Login {
	Login() {
	}

	WebDriver driver;
	Logger log = Logger.getLogger("ScriptLogger");
	Exceldata data = new Exceldata();

	@BeforeClass
	public void browser() throws EncryptedDocumentException, InvalidFormatException, IOException {
		System.setProperty("webdriver.chrome.driver", "D:\\Selenium\\Webdrivers\\chromedriver_win32\\chromedriver.exe");
		driver = new ChromeDriver();
		String URL = data.getCellData(1, 1);
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		driver.manage().window().maximize();
		driver.get(URL);
	}

	@Test(enabled = true, priority = 1)
	public void username() throws EncryptedDocumentException, InvalidFormatException, IOException {
		LoginPage loginpage = new LoginPage(driver);
		String Usernamedata1 = data.getCellData(1, 2);
		loginpage.username(Usernamedata1);
		Utilities.captureScreenShot(driver);
	}

	@Test(enabled = true, priority = 3)
	public void password()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		// Validating the Password
		Thread.sleep(2000);
		WebElement Passkeyword = LoginPage.pass(driver);
		String Passworddata = data.getCellData(1, 3);
		Assert.assertEquals(true, Passkeyword.isDisplayed());
		Passkeyword.sendKeys(Passworddata);
		Utilities.captureScreenShot(driver);
	}

	@Test(enabled = true, priority = 4)
	public void loginbutton() throws InterruptedException {
		// Validating the Login
		Thread.sleep(2000);
		WebElement Loginbutton = LoginPage.loginButton(driver);
		Assert.assertEquals(true, Loginbutton.isDisplayed());
		Loginbutton.click();
		log.info("Login Button Clicked");
		Utilities.captureScreenShot(driver);
	}

	@Test(enabled = true,priority = 2)
	public void verifydataentered()
			throws EncryptedDocumentException, InvalidFormatException, IOException, InterruptedException {
		WebElement Usernamekeyword = LoginPage.logindatacheck(driver);
		log.info(Usernamekeyword);
		String Enteredusername = Usernamekeyword.getAttribute("Value");
		log.info(Enteredusername);
		String Usernamedata = data.getCellData(1, 2);
		if (Enteredusername.equals(Usernamedata)) {
			log.info("Entered username value is" + Enteredusername);
		} else {
			log.info("Value is not equal to" + Usernamedata);
		}
		Thread.sleep(2000);
		Utilities.captureScreenShot(driver);
	}

	@AfterClass
	public void closebrowser() throws InterruptedException {
		driver.quit();
		log.info("closebrowser");
		Thread.sleep(5000);
	}
	@AfterSuite
	public void aftersuite() {
		log.info("After Suite Method");
	}
}
