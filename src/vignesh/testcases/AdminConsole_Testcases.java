package vignesh.testcases;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import vignesh.excel.Exceldata;
import vignesh.pageobjects.Admin_Console;
import vignesh.utils.Utilities;
public class AdminConsole_Testcases extends Login {
	Exceldata data = new Exceldata();
	AdminConsole_Testcases() {}
	@Test(enabled = true, priority = 5)
	public void postsSidebar() throws InterruptedException {
		Thread.sleep(2000);
		boolean side_posts = Admin_Console.postslink(driver).isDisplayed();
		if (side_posts) {
			Admin_Console.postslink(driver).click();
			log.info("Posts menu clicked");
		} else {
			log.info("Posts link not found");
		}
		Utilities.captureScreenShot(driver);
	}
	@Test(enabled = true, priority = 6)
	public void postsAddnew() throws InterruptedException {
		Thread.sleep(2000);
		boolean addnew = Admin_Console.postsAddnew(driver).isDisplayed();
		if (addnew) {
			Admin_Console.postsAddnew(driver).click();
			log.info(addnew + "is clicked");
		} else {
			log.info("Add new not found");
		}
		Utilities.captureScreenShot(driver);
	}
	@Test(enabled = true, priority = 7)
	public void postsAddnewTitle() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Thread.sleep(2000);
		boolean Title = Admin_Console.postsTitle(driver).isDisplayed();
		if (Title) {
			Thread.sleep(2000);
			String Titledata = data.getCellData(1, 4);
			WebElement addnew = Admin_Console.postsTitle(driver);
			log.info("Title Entered" + Titledata);
			Actions actions = new Actions(driver);
			actions.moveToElement(addnew);
			actions.click();
			actions.sendKeys(Titledata);
			actions.build().perform();
		} else {
			log.info("Title Not Entered");
		}
		Utilities.captureScreenShot(driver);
	}
	@Test(enabled = true, priority = 8)
	public void postsAddnewBody() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Thread.sleep(2000);
		boolean Title = Admin_Console.postsbodyiframe(driver).isDisplayed();
		if (Title) {
			Thread.sleep(2000);
			log.info("Iframe found");
			WebElement body_iframe = Admin_Console.postsbodyiframe(driver);
			driver.switchTo().frame(body_iframe);
			log.info("Switched to iframe");
			String Bodydata = data.getCellData(1, 5);
			Thread.sleep(1000);
			Admin_Console.postsbodyContent(driver).sendKeys(Bodydata);
			log.info("Body content Entered" + Bodydata);
			driver.switchTo().defaultContent();
		} else {
			log.info("Body content not Entered");
		}
		Utilities.captureScreenShot(driver);
	}
	@Test(enabled = true, priority = 9)
	public void postspreview() throws InterruptedException {
		Thread.sleep(2000);
		WebElement Preview = Admin_Console.postsPreview(driver);
		if (Preview != null) {
			Thread.sleep(2000);
			Preview.click();
			log.info(Preview);
		} else {
			log.info("Element is Absent");
		}
		Utilities.captureScreenShot(driver);
	}
	@SuppressWarnings("unlikely-arg-type")
	@Test(enabled = true, priority = 10)
	public void previewNewtab() throws InterruptedException {
		Thread.sleep(2000);
		String ConsoleTab = driver.getWindowHandle();
		ArrayList<String> newTab = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(newTab.get(1));
		Utilities.captureScreenShot(driver);
		Thread.sleep(2000);
		driver.close();
		driver.switchTo().window(ConsoleTab);
		newTab.remove(newTab);
	}
	@Test(enabled = true, priority = 11)
	public void addingtags() throws InterruptedException, EncryptedDocumentException, InvalidFormatException, IOException {
		Thread.sleep(2000);
		WebElement tags = Admin_Console.poststags(driver);
		if (tags != null) {
			String tag = data.getCellData(1, 6);
			tags.sendKeys(tag);
			Utilities.captureScreenShot(driver);
			log.info(tags + tag);
			Admin_Console.poststagsadd(driver).click();
			log.info("Add clicked");
		} else {
			log.info("Element is Absent");
		}
		Utilities.captureScreenShot(driver);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("scroll(400, 0)");
		postspreview();
		previewNewtab();
	}
	@Test(enabled = true, priority = 12)
	public void selectwidget() throws InterruptedException {
		Thread.sleep(2000);
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("scroll(100, 0)");
		boolean Link_radio = Admin_Console.postsWidgetlink(driver).isSelected();
		if (Link_radio == false) {
			Admin_Console.postsWidgetlink(driver).click();
			log.info("Radio button selected");
		} else {
			log.info("Radio  is already selected");
		}
		Utilities.captureScreenShot(driver);
	}
	@Test(enabled = true, priority = 13)
	public void screenoptions() throws InterruptedException {
		Thread.sleep(2000);
		WebElement Screen_optonButton = Admin_Console.screenoption(driver);
		if (Screen_optonButton != null) {
			Screen_optonButton.click();
			log.info(Screen_optonButton + "- Clicked");
		} else {
			log.info("Cannot find element" + Screen_optonButton);}
		Utilities.captureScreenShot(driver);
	}}
