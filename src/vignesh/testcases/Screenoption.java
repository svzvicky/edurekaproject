package vignesh.testcases;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.Test;
import vignesh.utils.Utilities;

public class Screenoption extends AdminConsole_Testcases {
	Screenoption() {}
	@Test(enabled = true, priority =14)
  public void formatcheck(){
		JavascriptExecutor js = (JavascriptExecutor)driver;
		js.executeScript("scroll(100, 0)");
		Utilities.captureScreenShot(driver);
		boolean Formatcheck = vignesh.pageobjects.Screenoptions.format(driver).isSelected();
	if (Formatcheck == true) {
		vignesh.pageobjects.Screenoptions.format(driver).click();
		log.info("Element unchecked");
	}else
	{
		log.info("Element Already unchecked");
	}
	Utilities.captureScreenShot(driver);
	}
	@Test(enabled = true, priority =15)
	  public void categoriescheck(){
			boolean Categoriescheck = vignesh.pageobjects.Screenoptions.categories(driver).isSelected();
		if (Categoriescheck == true) {
			vignesh.pageobjects.Screenoptions.categories(driver).click();
			log.info("Element unchecked");
		}else
		{
			log.info("Element Already unchecked");
		}
		Utilities.captureScreenShot(driver);
		}
	@Test(enabled = true, priority =16)
	  public void tagscheck(){
			boolean Tagscheck = vignesh.pageobjects.Screenoptions.tags(driver).isSelected();
		if (Tagscheck == true) {
			vignesh.pageobjects.Screenoptions.tags(driver).click();
			log.info("Element unchecked");
		}else
		{
			log.info("Element Already unchecked");
		}
		Utilities.captureScreenShot(driver);
		}
	@Test(enabled = true, priority =17)
	  public void imagecheck(){
			boolean imagecheck = vignesh.pageobjects.Screenoptions.featuredimage(driver).isSelected();
		if (imagecheck == true) {
			vignesh.pageobjects.Screenoptions.featuredimage(driver).click();
			log.info("Element unchecked");
		}else
		{
			log.info("Element Already unchecked");
		}
		Utilities.captureScreenShot(driver);
		}
	@Test(enabled = true, priority =18)
	  public void excerptcheck(){
			boolean Excerptcheck = vignesh.pageobjects.Screenoptions.excerpt(driver).isSelected();
		if (Excerptcheck == true) {
			vignesh.pageobjects.Screenoptions.excerpt(driver).click();
			log.info("Element unchecked");
		}else
		{
			log.info("Element Already unchecked");
		}
		Utilities.captureScreenShot(driver);
		}
	@Test(enabled = true, priority =19)
	  public void trackbackcheck(){
			boolean Trackbackcheck = vignesh.pageobjects.Screenoptions.trackbacks(driver).isSelected();
		if (Trackbackcheck == true) {
			vignesh.pageobjects.Screenoptions.trackbacks(driver).click();
			log.info("Element unchecked");
		}else
		{
			log.info("Element Already unchecked");
		}
		Utilities.captureScreenShot(driver);
		}
	@Test(enabled = true, priority =20)
	  public void cfieldscheck(){
			boolean Cfieldscheck = vignesh.pageobjects.Screenoptions.customFields(driver).isSelected();
		if (Cfieldscheck == true) {
			vignesh.pageobjects.Screenoptions.customFields(driver).click();
			log.info("Element unchecked");
		}else
		{
			log.info("Element Already unchecked");
		}
		Utilities.captureScreenShot(driver);
		}
	@Test(enabled = true, priority =21)
	  public void discussioncheck(){
			boolean Discussioncheck = vignesh.pageobjects.Screenoptions.discussion(driver).isSelected();
		if (Discussioncheck == true) {
			vignesh.pageobjects.Screenoptions.discussion(driver).click();
			log.info("Element unchecked");
		}else
		{
			log.info("Element Already unchecked");
		}
		Utilities.captureScreenShot(driver);
		}
	@Test(enabled = true, priority =22)
	  public void slugcheck(){
			boolean Slugcheck = vignesh.pageobjects.Screenoptions.slugs(driver).isSelected();
		if (Slugcheck == true) {
			vignesh.pageobjects.Screenoptions.slugs(driver).click();
			log.info("Element unchecked");
		}else
		{
			log.info("Element Already unchecked");
		}
		Utilities.captureScreenShot(driver);
		}
	@Test(enabled = true, priority =23)
	  public void authorcheck(){
			boolean Authorcheck = vignesh.pageobjects.Screenoptions.author(driver).isSelected();
		if (Authorcheck == true) {
			vignesh.pageobjects.Screenoptions.author(driver).click();
			log.info("Element unchecked");
		}else
		{
			log.info("Element Already unchecked");
		}
		Utilities.captureScreenShot(driver);
		}
	@Test(enabled = true, priority =24)
	  public void logout() throws InterruptedException{
		WebElement account = vignesh.pageobjects.Screenoptions.myaccount(driver);
		WebElement logout = vignesh.pageobjects.Screenoptions.myaccount(driver);
		if (account != null) {
			Actions actions = new Actions(driver);
			actions.moveToElement(account).build().perform();
			Utilities.captureScreenShot(driver);
			Thread.sleep(2000);
			actions.moveToElement(logout).build().perform();
			logout.click();
		}else {
			log.info("Element not found");
		}
	}
}

