package vignesh.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
	
	public LoginPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	private static WebElement element= null;
	
	  @FindBy(id = "user_login")
	  @CacheLookup
	  private WebElement usernamecached;
	  
	  public void username(String usernamedata) {
		  usernamecached.sendKeys(usernamedata);
	  }
	  
	  public String usernamedata() {
		  return usernamecached.getAttribute("usernamecached");
	  }
	
	public static WebElement pass(WebDriver driver) {
		 element = driver.findElement(By.id("user_pass"));
		 return element;
	}
	
	public static WebElement logindatacheck(WebDriver driver) {
		 element = driver.findElement(By.id("user_login"));
		 return element;
	}
	
	public static WebElement loginButton(WebDriver driver) {
		 element = driver.findElement(By.id("wp-submit"));
		 return element;
	}
	public static WebElement lostpassword(WebDriver driver) {
		element = driver.findElement(By.linkText("Lost your password?"));
		return element;
	}
	
}
