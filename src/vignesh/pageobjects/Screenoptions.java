package vignesh.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Screenoptions {
	Screenoptions() {}
	private static WebElement element = null;

	public static WebElement format(WebDriver driver) {
		element = driver.findElement(By.id("formatdiv-hide"));;
		return element;
	}
	public static WebElement categories(WebDriver driver) {
		element = driver.findElement(By.id("categorydiv-hide"));;
		return element;
	}
	public static WebElement tags(WebDriver driver) {
		element = driver.findElement(By.id("tagsdiv-post_tag-hide"));;
		return element;
	}
	public static WebElement featuredimage(WebDriver driver) {
		element = driver.findElement(By.id("postimagediv-hide"));;
		return element;
	}
	public static WebElement excerpt(WebDriver driver) {
		element = driver.findElement(By.id("postexcerpt-hide"));;
		return element;
	}
	public static WebElement trackbacks(WebDriver driver) {
		element = driver.findElement(By.id("trackbacksdiv-hide"));;
		return element;
	}
	public static WebElement customFields(WebDriver driver) {
		element = driver.findElement(By.id("postcustom-hide"));;
		return element;
	}
	public static WebElement discussion(WebDriver driver) {
		element = driver.findElement(By.id("commentstatusdiv-hide"));;
		return element;
	}
	public static WebElement slugs(WebDriver driver) {
		element = driver.findElement(By.id("slugdiv-hide"));;
		return element;
	}
	public static WebElement author(WebDriver driver) {
		element = driver.findElement(By.id("authordiv-hide"));;
		return element;
	}
	public static WebElement myaccount(WebDriver driver) {
		element = driver.findElement(By.id("wp-admin-bar-my-account"));;
		return element;
	}
	public static WebElement myaccountlogout(WebDriver driver) {
		element = driver.findElement(By.id("wp-admin-bar-logout"));;
		return element;
	}
}
	
