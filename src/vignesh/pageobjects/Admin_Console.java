package vignesh.pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Admin_Console {
	Admin_Console() {}
	private static WebElement element = null;

	public static WebElement postslink(WebDriver driver) {
		element = driver.findElement(By.xpath("//*[@id='menu-posts']/a/div[3]"));;
		return element;
	}
	public static WebElement postsAddnew(WebDriver driver) {
		element = driver.findElement(By.xpath("//*[@id=\"wpbody-content\"]/div[3]/a"));;
		return element;
	}
	public static WebElement postsTitle(WebDriver driver) {
			element = driver.findElement(By.id("title-prompt-text"));;
			return element;
	}
	public static WebElement postsbodyiframe(WebDriver driver) {
		element = driver.findElement(By.id("content_ifr"));;
		return element;
}
	public static WebElement postsbodyContent(WebDriver driver) {
		element = driver.findElement(By.xpath("//*[@id='tinymce']"));;
		return element;
}
	public static WebElement postsPreview(WebDriver driver) {
		element = driver.findElement(By.xpath("//*[@id='post-preview']"));;
		return element;
}
	public static WebElement poststags(WebDriver driver) {
		element = driver.findElement(By.xpath("//*[@id='new-tag-post_tag']"));
		return element;
}
	public static WebElement poststagsadd(WebDriver driver) {
		element = driver.findElement(By.xpath("//*[@id=\"post_tag\"]/div/div[2]/p/input[2]"));
		return element;
}
	public static WebElement postsWidgetlink(WebDriver driver) {
		element = driver.findElement(By.id("post-format-link"));
		return element;
}
	public static WebElement screenoption(WebDriver driver) {
		element = driver.findElement(By.id("show-settings-link"));
		return element;
}
}